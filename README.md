# ComServer
Share COM over network.

## How to
1. Download [server_rs232.exe](http://www.javiervalcarce.eu/html/sw-access-to-serial-port-from-internet-en.html).
2. ``cmd > server_rs232.exe 9100 COM1``
3. Done.

## References
- http://www.javiervalcarce.eu/html/sw-access-to-serial-port-from-internet-en.html