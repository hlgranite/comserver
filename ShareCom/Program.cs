﻿using System.Diagnostics;

namespace ShareCom
{
    class Program
    {
        static void Main(string[] args)
        {
            // start a hidden console
            ProcessStartInfo si = new ProcessStartInfo();
            si.CreateNoWindow = true;
            si.FileName = "sharecom.bat";
            si.UseShellExecute = false;
            System.Diagnostics.Process.Start(si);

            //Process p = new Process();
            //p.File
            //p.StartInfo.CreateNoWindow = true;
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //p.Start();

            //Console.Write("Enter any key to exit");
            //Console.Read();
        }
    }
}